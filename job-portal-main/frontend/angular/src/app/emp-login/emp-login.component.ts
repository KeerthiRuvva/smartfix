import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-emp-login',
  templateUrl: './emp-login.component.html',
  styleUrls: ['./emp-login.component.css']
})
export class EmpLoginComponent {
  employee: any;

  constructor(private router: Router, private service: DjpserviceService, private toastr: ToastrService) {


  }


  async validateLogin(emp: any) {
    console.log(emp);
    localStorage.setItem('emailIdreg', emp.emailId);
    localStorage.setItem('passwordreg', emp.password);
    if (emp.EmailId == "Admin@g" && emp.password == "Admin") {
      this.service.setEmpLoggedIn();
      this.toastr.success("login successfull");
      this.router.navigate(['jobs']);
    } else {
      await this.service.empLogin(emp).then((data: any) => {
        this.service.setEmpLoggedIn();
        console.log(data);
        this.employee = data;

      });
      if (this.employee != null) {

        this.router.navigate(['jobs']);
      }



    }

  }


}
