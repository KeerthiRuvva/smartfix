import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { OtpComponent } from './otp/otp.component';
import { LogoutComponent } from './logout/logout.component';
import { EmpLoginComponent } from './emp-login/emp-login.component';
import { EmpRegComponent } from './emp-reg/emp-reg.component';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { JqueryDirectiveDirective } from './jquery-directive.directive';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { JobViewComponent } from './job-view/job-view.component';
import { ForgetPassComponent } from './forget-pass/forget-pass.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MatCardModule } from '@angular/material/card';
import { JobshowComponent } from './jobshow/jobshow.component';
import { FilterjobsComponent } from './filterjobs/filterjobs.component';
import { JobapplicationComponent } from './jobapplication/jobapplication.component';
import { ProfileComponent } from './profile/profile.component';
import { LogoutempComponent } from './logoutemp/logoutemp.component';
import { ForgetEmpComponent } from './forget-emp/forget-emp.component';
import { ResetEmpComponent } from './reset-emp/reset-emp.component';
import { AboutusComponent } from './aboutus/aboutus.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    OtpComponent,
    LogoutComponent,
    EmpLoginComponent,
    EmpRegComponent,
    JobPostsComponent,
    LandingpageComponent,
    JqueryDirectiveDirective,
    FooterComponent,
    ContactComponent,
    HeaderComponent,
    JobViewComponent,
    ForgetPassComponent,
    ResetPasswordComponent,
    JobshowComponent,
    FilterjobsComponent,
    JobapplicationComponent,
    ProfileComponent,
    LogoutempComponent,
    ForgetEmpComponent,
    ResetEmpComponent,
    AboutusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
