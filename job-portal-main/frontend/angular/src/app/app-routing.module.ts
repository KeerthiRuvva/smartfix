import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { OtpComponent } from './otp/otp.component';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { EmpLoginComponent } from './emp-login/emp-login.component';
import { EmpRegComponent } from './emp-reg/emp-reg.component';
import { JobPostsComponent } from './job-posts/job-posts.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { ContactComponent } from './contact/contact.component';
import { JobViewComponent } from './job-view/job-view.component';
import { ForgetPassComponent } from './forget-pass/forget-pass.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { JobshowComponent } from './jobshow/jobshow.component';
import { FilterjobsComponent } from './filterjobs/filterjobs.component';
import { JobapplicationComponent } from './jobapplication/jobapplication.component';
import { ProfileComponent } from './profile/profile.component';
import { LogoutempComponent } from './logoutemp/logoutemp.component';
import { ForgetEmpComponent } from './forget-emp/forget-emp.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ResetEmpComponent } from './reset-emp/reset-emp.component';



const routes: Routes = [

  { path: "", component: LandingpageComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "login/register", component: RegisterComponent },
  { path: "otp", component: OtpComponent },
  { path: "home", component: HeaderComponent },
  { path: "logout", component: LogoutComponent },
  { path: "logoutemp", component: LogoutempComponent },
  { path: "emplogin", component: EmpLoginComponent },
  { path: "empreg", component: EmpRegComponent },
  { path: "jobs", component: JobPostsComponent },
  { path: "empreg/emplogin", component: EmpLoginComponent },
  { path: "empreg/emplogin/empreg", component: EmpRegComponent },
  { path: "emplogin/empreg", component: EmpRegComponent },
  { path: "emplogin/empreg/emplogin", component: EmpLoginComponent },
  { path: "contact", component: ContactComponent },
  { path: "landing", component: LandingpageComponent },
  { path: "resetpassword", component: ResetPasswordComponent },
  { path: "login/forget", component: ForgetPassComponent },
  { path: "login/jobsview", canActivate: [authGuard], component: JobViewComponent },
  { path: "jobsview", canActivate: [authGuard], component: JobViewComponent },
  { path: 'applyjob', canActivate: [authGuard], component: JobapplicationComponent },
  { path: "filterjobs", canActivate: [authGuard], component: FilterjobsComponent },
  { path: "jobview/jobshow", canActivate: [authGuard], component: JobshowComponent },
  { path: "jobshow", canActivate: [authGuard], component: JobshowComponent },
  { path: "profile", canActivate: [authGuard], component: ProfileComponent },
  { path: "emplogin/forgetemp", component: ForgetEmpComponent },
  { path: "aboutus", component: AboutusComponent },
  { path: "job",canActivate:[authGuard], component: JobViewComponent },
  {path:"resetpassword/login",component:LoginComponent},
  {path:"resetemp",component:ResetEmpComponent},
  {path:"resetpassword/emplogin",component:EmpLoginComponent},
  {path:"emplogin/register",component:EmpRegComponent}
 



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
