import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';
import { Serializer } from '@angular/compiler';

@Component({
  selector: 'app-filterjobs',
  templateUrl: './filterjobs.component.html',
  styleUrls: ['./filterjobs.component.css']
})
export class FilterjobsComponent implements OnInit {
  job: any;
  location1: string = "";
  skills: any;

  constructor(private router: Router, private service: DjpserviceService) {
    const userString = localStorage.getItem('location');
    console.log(userString);
    // this.location1 = userString;
    this.service.location(userString).subscribe((data: any) => {
      console.log(data);
      this.job = data;
    });
    const skill = localStorage.getItem('skills');
    console.log(skill);

    service.skills(skill).subscribe((skills: any) => {
      console.log(skills);
      this.job = skills;
    });


  }
  ngOnInit() {




  }

  submit(data: any) {
    localStorage.setItem('Data', JSON.stringify(data));
    this.router.navigate(['jobshow']);
  }
}
