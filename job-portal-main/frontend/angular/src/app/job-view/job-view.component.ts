import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';
import { ToastrService } from 'ngx-toastr';

declare var jQuery: any;


@Component({
  selector: 'app-job-view',
  templateUrl: './job-view.component.html',
  styleUrls: ['./job-view.component.css']
})
export class JobViewComponent {

  job: any;
  data: any;

  constructor(private router: Router, private service: DjpserviceService, private toastr: ToastrService) {
    this.job = {
      "postId": null,
      "jobTitle": "",
      "numberOfOpenings": null,
      "location": "",
      "experience": "",
      "salary": null,
      "jobDescription": "",
      "skills": "",
      "age": null,
      "preferredLanguage": "",
      "degree_specialisation": "",
      "certification": "",
      "jobTimings": "",
      "companyName": "",
      "personName": "",
      "emailId": "",
      "contact_Person_Profile": "",
      "jobAddress": "",
      "phoneNumber": ""
    }
    this.service.getAll().subscribe((data) => {
      //console.log(data);
      this.job = data;

    });
    const userString = localStorage.getItem('Data');
    if (userString) {
      this.data = JSON.parse(userString);
    }
  }
  getall(data: any) {
    console.log(data);
    // const data1 = data;
    // this.router.navigate(['jobdetails/${data1}']);

    // this.service.setData(data);
    localStorage.setItem('Data', JSON.stringify(data));
    this.router.navigate(['jobshow']);
  }

  navigate() {
    this.router.navigate(['jobshow']);
  }

  submit(data: any) {
    localStorage.setItem('Data', JSON.stringify(data));
    this.router.navigate(['jobshow']);
  }

  // ngOnInit() {

  //   const userString = localStorage.getItem('Data');
  //   if (userString) {
  //     this.data = JSON.parse(userString);
  //   }
  // }
}
