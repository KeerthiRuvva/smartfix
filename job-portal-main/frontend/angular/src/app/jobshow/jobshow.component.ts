import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-jobshow',
  templateUrl: './jobshow.component.html',
  styleUrls: ['./jobshow.component.css']
})
export class JobshowComponent{

  data:any;
  jobData:any;
  constructor(private router:Router,private route:ActivatedRoute,private service:DjpserviceService){

    const userString = localStorage.getItem('Data');
    if(userString){
      this.data = JSON.parse(userString);
    }

  }
  Apply(data:any){

    this.router.navigate(['applyjob']);
    
  }
  location(ButtonValue:string):void{
    console.log(ButtonValue);

    const userdata = localStorage.setItem('location',ButtonValue);
    this.router.navigate(['filterjobs']);
  }

  skills(ButtonValue:string){
    console.log(ButtonValue);

    const userdata = localStorage.setItem('skills',ButtonValue);
    this.router.navigate(['filterjobs']);

  }

}
