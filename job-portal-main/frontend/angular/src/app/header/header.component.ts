import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private service: DjpserviceService) {

  }

  logout(): boolean {
    return this.service.getUserLoggedStatus();
  }

  loginRegisterdis(): boolean {

    if (this.service.getUserLoggedStatus() || this.service.getEmpLoggedStatus()) {
      return true;
    }
    return false;
  }

  profile() {

    if (this.service.getUserLoggedStatus() || !this.service.getEmpLoggedStatus) {
      return true;
    } else {
      return false;
    }
  }

  logoutemp() {
    return this.service.getEmpLoggedStatus();
  }


}
