import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-logoutemp',
  templateUrl: './logoutemp.component.html',
  styleUrls: ['./logoutemp.component.css']
})
export class LogoutempComponent {

  constructor(private router: Router, private service: DjpserviceService) {
    service.setEmpLoggedOut();
    router.navigate(['emplogin']);
  }
}
