import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: any;
  User: any;
  isuserlogin: boolean;
  reguser: any;



  captchaText: string = '';
  userInput: string = '';
  captchaFeedback: string = '';
  isCaptchaCorrect: boolean = false;
  isOtherFieldsValid: boolean = false;


  constructor(private router: Router, private service: DjpserviceService, private toaster: ToastrService) {
    this.isuserlogin = false;
    this.refreshCaptcha();
    this.isCaptchaCorrect = false;
    this.User = {
      "fullName": "",
      "gender": "",
      "phoneNumber": "",
      "emailId": "",
      "password": ""
    }
  }

  async validateLogin(loginForm: any) {
    console.log(loginForm);
    localStorage.setItem('emailId', loginForm.emailId);
    localStorage.setItem('password', loginForm.password);
    if (loginForm.emailId == "Admin" && loginForm.password == "Admin") {
      this.service.setUserLoggedIn();
      this.user = loginForm;
    } else {
      await this.service.loginUser(loginForm).then((data: any) => {

        this.user = data;
        console.log(data + "new");
        console.log(this.user);
      });

    }
    if (this.user != null) {
      this.service.setUserLoggedIn();
      this.router.navigate(['jobsview']);
    } else {
      this.toaster.error("invaild credentials");
    }




  }
  validateRegister(regForm: any) {

    this.User.fullName = regForm.fullName;
    this.User.gender = regForm.gender;
    this.User.phoneNumber = regForm.phoneNumber;
    this.User.password = regForm.password;
    this.User.emailId = regForm.emailId;
    console.log(this.User);

    this.service.registeruser(this.User).subscribe((userdata: any) => {

      console.log(userdata);
      this.reguser = userdata;

    });
    if (this.reguser != null) {
      this.toaster.success("successfully registered..");
    } else {
      this.toaster.error("Invaild credential");
    }

  }

  isPasswordValid(password: string): boolean {
    const uppercase = /[A-Z]/;
    if (!uppercase.test(password)) {
      return false;
    }

    const specialCharacter = /[$&+,:;=?@#|'<>.^*()%!-]/;
    if (!specialCharacter.test(password)) {
      return false;
    }
    
    const numbers = /[0-9]/;
    if (!numbers.test(password)) {
      return false;
    }
    return true;
  }

  isPhoneNumberValid(phoneNumber: string): boolean {

    const pattern = /^[6-9]\d{9}$/;
    if (!pattern.test(phoneNumber)) {
      return false;
    }
    return true;

  }

  ngOnInit(): void {
    this.refreshCaptcha();
  }

  refreshCaptcha() {
    this.captchaText = this.generateRandomLetters(6); // Generate a 6-character CAPTCHA
    this.userInput = ''; // Clear user input
    this.captchaFeedback = ''; // Clear feedback
    this.isCaptchaCorrect = false;
  }

  generateRandomLetters(length: number): string {
    const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; i++) {
      result += letters.charAt(Math.floor(Math.random() * letters.length));
    }
    return result;
  }

  checkCaptcha() {
    if (this.userInput === this.captchaText) {
      this.captchaFeedback = 'CAPTCHA Correct';
      this.isCaptchaCorrect = true;
    } else {
      this.captchaFeedback = 'CAPTCHA Incorrect';
      this.isCaptchaCorrect = false;

    }
  }
}
