import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-jobapplication',
  templateUrl: './jobapplication.component.html',
  styleUrls: ['./jobapplication.component.css']
})
export class JobapplicationComponent {

  applyform1: any;
  constructor(private service: DjpserviceService, private toast: ToastrService) {
    this.applyform1 = {
      "fullName": "",
      "dateOfBirth": "2023-10-19",
      "gender": "",
      "emailId": "",
      "phoneNumber": "",
      "address": "",
      "schoolName": "",
      "collPercentage": "",
      "graduationInstitute": "",
      "graduationPercentage": "",
      "collegeName": "",
      "linkedInURL": "",
      "schoolPercentage": "",
      "graduationPassedOut": ""
    }
  }


  submit(applyform: any) {
    console.log(applyform);

    this.applyform1.fullName = applyform.name;
    this.applyform1.dateOfBirth = applyform.bdate;
    this.applyform1.gender = applyform.gender;
    this.applyform1.emailId = applyform.Email;
    this.applyform1.phoneNumber = applyform.Phone;
    this.applyform1.address = applyform.Address;
    this.applyform1.schoolName = applyform.School;
    this.applyform1.collPercentage = applyform.CollegePercentage;
    this.applyform1.graduationInstitute = applyform.Graduation;
    this.applyform1.graduationPercentage = applyform.GraduationPercentage;
    this.applyform1.collegeName = applyform.Inter;
    this.applyform1.linkedInURL = applyform.LinkedIn;
    this.applyform1.schoolPercentage = applyform.SchoolPercentage;
    this.applyform1.graduationPassedOut = applyform.PassedOut;
    console.log(this.applyform1);

    this.service.jobApply(this.applyform1).subscribe((data: any) => {
      console.log(data);
      if (data != null) {
        this.toast.success("successfully applied");
      } else {
        this.toast.error("Error");
      }
    });





  }
}
