import { JsonPipe } from '@angular/common';
import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  emailId: any;
  password: any;
  user: any;
  constructor(private service: DjpserviceService) {
    this.emailId = localStorage.getItem("emailId");
    this.password = localStorage.getItem("password");



    service.loginUser1(this.emailId, this.password).subscribe((data: any) => {
      this.user = data;
    });

  }
}
