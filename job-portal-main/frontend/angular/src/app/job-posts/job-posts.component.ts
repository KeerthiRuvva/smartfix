import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-job-posts',
  templateUrl: './job-posts.component.html',
  styleUrls: ['./job-posts.component.css']
})
export class JobPostsComponent {

  job: any;
  jobs: any;
  constructor(private service: DjpserviceService, private router: Router, private toastr: ToastrService) {
    this.job = {
      "postId": null,
      "jobTitle": "",
      "numberOfOpenings": null,
      "location": "",
      "experience": "",
      "salary": null,
      "jobDescription": "",
      "skills": "",
      "age": null,
      "preferredLanguage": "",
      "degree_specialisation": "",
      "certification": "",
      "jobTimings": "",
      "lastDate": "",
      "companyName": "",
      "personName": "",
      "emailId": "",
      "contact_Person_Profile": "",
      "jobAddress": "",
      "phoneNumber": ""
    }
  }


  async jobpost(regForm: any) {
    //console.log(regForm);

    this.job.jobTitle = regForm.Title;
    this.job.numberOfOpenings = regForm.NumberOfOpenings;
    this.job.location = regForm.indianCitiesDropdown;

    this.job.experience = regForm.jobexperience;
    this.job.salary = regForm.salary;
    this.job.jobDescription = regForm.description;
    this.job.skills = regForm.skills;

    this.job.age = regForm.Age;
    this.job.preferredLanguage = regForm.preferredLanguage;
    this.job.degree_specialisation = regForm.degree_Specialisation;
    this.job.certification = regForm.Certification;


    this.job.jobTimings = regForm.Timings;
    this.job.lastDate = regForm.lastdate;

    this.job.companyName = regForm.companyName;
    this.job.personName = regForm.personName;
    this.job.emailId = regForm.EmailId;
    this.job.contact_Person_Profile = regForm.PersonProfile;
    this.job.jobAddress = regForm.Jobaddress;
    this.job.phoneNumber = regForm.PhoneNumber;



    console.log(this.job);


    await this.service.jobPost(this.job).then((data: any) => {

      this.jobs = data;



    });
    if (this.jobs != null) {
      this.toastr.success("Jobposted successfully");
      this.jobs = null;
    } else {
      this.toastr.error("Jobpost failed");
    }

  }
  isPasswordValid(password: string): boolean {
    const uppercase = /[A-Z]/;
    if (!uppercase.test(password)) {
      return false;
    }

    const specialCharacter = /[$&+,:;=?@#|'<>.^*()%!-]/;
    if (!specialCharacter.test(password)) {
      return false;
    }
    const numbers = /[0-9]/;
    if (!numbers.test(password)) {
      return false;
    }

    return true;
  }

  isPhoneNumberValid(phoneNumber: string): boolean {

    const pattern = /^[6-9]\d{9}$/;
    if (!pattern.test(phoneNumber)) {
      return false;
    }

    return true;

  }

}
