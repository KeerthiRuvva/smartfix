import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { DjpserviceService } from './djpservice.service';

export const authGuard: CanActivateFn = (route, state) => {
  
  let service = inject(DjpserviceService);
  return service.getUserLoggedStatus();
};


