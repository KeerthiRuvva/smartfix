import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

  constructor(private router: Router, private service: DjpserviceService) {
    this.service.setUserLoggedOut();
    router.navigate(['login']);
  }
}
