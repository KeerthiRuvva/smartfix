  import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DjpserviceService {

  User: any;
  user: any;
  isUserLogged: boolean;
  isEmpLogged: boolean;

  private dataSubject = new BehaviorSubject<any>(null);
  data$ = this.dataSubject.asObservable();

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.isEmpLogged = false;
    this.User = {
      "id": "",
      "fullName": "",
      "gender": "",
      "phoneNumber": "",
      "emailId": "",
      "password": ""
    }

  }

  // registerUser(user: any) {
  //   return this.http.post('http://localhost:8085/registerUser', user);
  // }
  registeruser(user: any) {
    return this.http.post('http://localhost:8085/registeruser', user);
  }
  registerotp(otp: any) {
    return this.http.post('emailVerify' + '/' + otp, otp);
  }

  loginUser(user: any) {
    return this.http.get("http://localhost:8085/userLogin/" + user.emailId + "/" + user.password).toPromise();
  }

  loginUser1(emailId: any,password:any) {
    return this.http.get("userLogin/" + emailId + "," + password);
  }

  empLogin(emp: any) {
    return this.http.get("http://localhost:8085/EmployeeLogin/" + emp.EmailId + "," + emp.password).toPromise();
  }
  empReg(emp: any) {
    return this.http.post("http://localhost:8085/EmployeeRegister", emp);
  }
  jobPost(job: any) {
    return this.http.post("http://localhost:8085/jobReg", job).toPromise();
  }

  forgetOtp(emailId: any) {
    return this.http.post("http://localhost:8085/getEmailOtp", emailId);
  }

  otpVerify(emailId: any, otp: any) {
    return this.http.put("http://localhost:8085/validateEmailOtp/" + emailId + "/" + otp, null);
  }

  resetpassword(emailid: any, password: any) {
    return this.http.put("http://localhost:8085/passwordReset/" + emailid + "," + password, null);
  }

  createUser(user: FormData) {
    return this.http.post("http://localhost:8085/reg", user);
  }

  getAll() {
    return this.http.get("http://localhost:8085/getAll");
  }

  jobApply(user: any) {
    return this.http.post("http://localhost:8085/jobApply", user);
  }
  //filters 
  //By location
  location(location: any) {
    return this.http.get("http://localhost:8085/findByLocation/" + location);
  }
  
  skills(skills:any){
    return this.http.get("http://localhost:8085/findBySkills/"+skills);

  }

  forgetOtpEmp(emailId: any) {
    return this.http.post("http://localhost:8085/getEmailOtpemp", emailId);
  }

  otpVerifyEmp(emailId: any, otp: any) {
    return this.http.put("http://localhost:8085/validateEmailemp/" + emailId + "/" + otp, null);
  }

  resetpasswordEmp(emailid: any, password: any) {
    return this.http.put("http://localhost:8085/passwordResetemp/" + emailid + "," + password, null);
  }


  setUserLoggedIn() {
    this.isUserLogged = true;
  }

  setUserLoggedOut() {
    this.isUserLogged = false;
  }
  setEmpLoggedIn() {
    this.isEmpLogged = true;
  }

  setEmpLoggedOut() {
    this.isEmpLogged = false;
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }
  getEmpLoggedStatus(): boolean {
    return this.isEmpLogged;
  }

  setData(data: any) {
    this.dataSubject.next(data);
  }

}



