import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-emp-reg',
  templateUrl: './emp-reg.component.html',
  styleUrls: ['./emp-reg.component.css']
})
export class EmpRegComponent {

  employee: any;

  constructor(private service: DjpserviceService, private router: Router, private toastr: ToastrService) {

    this.employee = {
      "fullName": "",
      "gender": "",
      "phoneNumber": "",
      "emailId": "",
      "password": ""
    }
  }
  validateRegister(regForm: any) {

    this.employee.fullName = regForm.fullName;
    this.employee.gender = regForm.gender;
    this.employee.phoneNumber = regForm.phoneNumber;
    this.employee.password = regForm.password;
    this.employee.emailId = regForm.emailId;
    console.log(this.employee);


    // this.service.registerUser(this.User).subscribe((userdata:any)=>{
    //   console.log(userdata);
    // });
    this.service.empReg(this.employee).subscribe((userdata: any) => {
      console.log(userdata);
    });
    this.router.navigate(['emplogin']);
    // this.router.navigate(['otp']);

  }




  isPasswordValid(password: string): boolean {
    const uppercase = /[A-Z]/;
    if (!uppercase.test(password)) {
      return false;
    }

    const specialCharacter = /[$&+,:;=?@#|'<>.^*()%!-]/;
    if (!specialCharacter.test(password)) {
      return false;
    }
    const numbers = /[0-9]/;
    if (!numbers.test(password)) {
      return false;
    }

    return true;
  }

  isPhoneNumberValid(phoneNumber: string): boolean {

    const pattern = /^[6-9]\d{9}$/;
    if (!pattern.test(phoneNumber)) {
      return false;
    }

    return true;

  }

}
