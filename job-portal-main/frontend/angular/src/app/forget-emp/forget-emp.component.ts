import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DjpserviceService } from '../djpservice.service';

@Component({
  selector: 'app-forget-emp',
  templateUrl: './forget-emp.component.html',
  styleUrls: ['./forget-emp.component.css']
})
export class ForgetEmpComponent {

  emailId: string = '';
  Otp: string = '';
  otp: any
  verify: any;



  constructor(private router: Router, private service: DjpserviceService, private toaster: ToastrService) {

  }



  forgetOtp(emailId: String) {
    console.log(emailId);
    this.service.forgetOtpEmp(emailId).subscribe((data: any) => {

      console.log();

      this.otp = data;

      if (this.otp != null) {
        this.toaster.success("otp sent");
      }
      else {
        this.toaster.error("Wrong emaild");
      }

    });

  }
  forgetpass(emailId: string, otp: string) {
    console.log(emailId, otp);

    localStorage.setItem("emailIdreg", emailId);
    this.service.otpVerifyEmp(emailId, otp).subscribe((data: any) => {
      console.log(data);
      this.verify = data;
      if (this.verify != null) {
        this.router.navigate(['resetemp']);
      } else {
        this.toaster.error("Wrong OTP");
      }

    });

  }
}
