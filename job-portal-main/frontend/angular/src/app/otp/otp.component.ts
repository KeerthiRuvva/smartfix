import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent {


  constructor(private service: DjpserviceService, private router: Router) {

  }
  
  validateotp(otpform: any) {
    console.log(otpform);

    this.service.registerotp('otpform.otp').subscribe((data: any) => {
      console.log(otpform);

      console.log(data);
      alert("success");
      this.router.navigate(['home']);
    });

  }
}