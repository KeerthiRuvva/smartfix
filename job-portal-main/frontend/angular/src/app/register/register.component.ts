import { Component } from '@angular/core';
import { DjpserviceService } from '../djpservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  First: any;
  employee: any;
  Firstname: any;
  JobApplication: any;
  Lastname: any;
  password: any;
  User:any;

  constructor(private service:DjpserviceService, private router:Router){

    this.User={
      "fullName": "",
      "gender": "",
      "phoneNumber":"",
      "emailId": "",
      "password": ""
    }
  }
  validateRegister(regForm:any){
    
    this.User.fullName = regForm.fullName;
    this.User.gender = regForm.gender;
    this.User.phoneNumber = regForm.phoneNumber;
    this.User.password = regForm.password;
    this.User.emailId = regForm.emailId;
    console.log(this.User);

    
    // this.service.registerUser(this.User).subscribe((userdata:any)=>{
    //   console.log(userdata);
    // });
    this.service.registeruser(this.User).subscribe((userdata:any)=>{
      this.router.navigate(['login']);
      console.log(userdata);
      
    });
    
    // this.router.navigate(['otp']);

  }

  ngOnInit(){
   
  }



  isPasswordValid(password: string): boolean {
    const uppercase = /[A-Z]/;
    if (!uppercase.test(password)) {
      return false;
    }

    const specialCharacter = /[$&+,:;=?@#|'<>.^*()%!-]/;
    if (!specialCharacter.test(password)) {
      return false;
    }
    const numbers = /[0-9]/;
    if(!numbers.test(password)){
      return false;
    }

    return true; 
  }

  isPhoneNumberValid(phoneNumber: string):boolean{

    const pattern = /^[6-9]\d{9}$/;
    if (!pattern.test(phoneNumber)){
      return false;
    }

    return true;

  }
}
